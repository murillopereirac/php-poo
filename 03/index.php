<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP POO</title>
</head>
<body>
<pre>
<?php

    require_once 'Caneta.php';
    $c1 = new Caneta;
    $c1->modelo = "BIC Cristal";
    $c1->cor = "Azul";
    $c1->destampar();
    print_r($c1);
    $c1->rabiscar();



?>
</pre>


</body>
</html>


